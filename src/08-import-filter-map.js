import heroes, { owners } from './data/heroes';

console.log(heroes);

const [ batman, spiderman, superman, flash, wolverine ] = heroes;

console.log(batman)


export const getHeroeById = (id, Arr) => {
    return Arr.find(heroe => heroe.id === id);
}

console.log(getHeroeById(2, heroes));

export const getHeroesByOwner = (owner, Arr) => {
    return Arr.filter(heroe => heroe.owner === owner);
}

console.log(getHeroesByOwner('DC', heroes));

