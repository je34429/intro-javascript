import heroes from './data/heroes'
import {getHeroeById} from './08-import-filter-map';

console.log(heroes);
const promesa = new Promise( (resolve, reject)=> {
    const timeout = 2000;
    setTimeout(() => {
        resolve();
    }, timeout);
});

promesa.then(()=>{
    console.log('promesa exitosa 2 segundos despues');
})
.catch(()=>{
    console.log('error en la promesa');
})
.finally(()=>{
    console.log('ejecución de la promesa finalizada');
});


/* const heroesP = new Promise((resolve, reject)=>{
    const heroe = getHeroeById(2, heroes);
    resolve(heroe);
    //reject("no se encontró el heroe");
});

heroesP.then((heroe)=>{
    console.log('heroe', heroe);
}).catch((error)=>{
    console.warn(error);
}).finally(()=>{
    console.log('consulta del heroe finalizada');
}); */

const imprimirHeroe = function(heroe){
    return console.log(heroe);
}

const manejarError = function(error){
    return console.warn(error)
}

const getHeroeByIdAsync = (id)=>{

    return new Promise((resolve, reject)=>{
        const heroe = getHeroeById(2, heroes);
        
        heroe ? resolve(heroe) : reject("no se encontró el heroe");
        
    });
};

getHeroeByIdAsync(10).then((heroe)=>{
    imprimirHeroe(heroe);
}).catch((error)=>{
    manejarError(error);
}).finally(()=>{
    console.log('consulta del heroe finalizada');
});
