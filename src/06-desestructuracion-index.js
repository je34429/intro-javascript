const persona = {
    nombre:'tonny',
    edad:45,
    clave:'ironman',
    latlng:{
        lat:14.322,
        lng: -34.2334,
    }
}

/*
desestructuración sencilla
const { nombre, edad, clave } =  persona;
console.log(nombre);
console.log(edad);
console.log(clave);
*/

//para renombrar la variable pero asignarle el valor original
const { nombre:nombre2, edad:edad2, clave:clave2 } =  persona;
console.log(nombre2);
console.log(edad2);
console.log(clave2);


const retornarPersona = ({nombre, edad, clave, pordefecto=0}) => {
    console.log(nombre,edad,clave, pordefecto);
}

retornarPersona(persona);


const botzer = ({nombre, edad, clave, pordefecto=0, latlng}) => {
    return {
        nombreClave: clave,
        anios: edad,
        latlng
    }
}
 

const { nombreClave, anios, latlng:{ lat, lng } } = botzer( persona );;

console.log(nombreClave, anios);
console.log(lat, lng);

