/* const apiKey = 'EfDOLlnpramCKIbbsr3Da2liVucCc6a9';

const  peticion = fetch(`http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);

peticion.then( resp => resp.json() )
.then( ({data}) => {
    console.log(data);
    const { url } = data.images.original;
    const img = document.createElement('img');
    img.src = url;
    document.body.append( img );
}).catch(console.warn); */

const getImagen = async() =>{
    try{
        const apiKey = 'EfDOLlnpramCKIbbsr3Da2liVucCc6a9';
        const  resp = await fetch(`http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
        const {data} = await resp.json();
        const imagen = document.createElement('img');
        console.log(data);
        const { url } = data.images.original;
        imagen.src = url;
        document.body.append( imagen );
    }catch(error){
        //manejo del error 
        console.warn(error);
    }
     
};

getImagen();
